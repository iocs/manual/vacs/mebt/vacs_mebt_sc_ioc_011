# IOC for MEBT vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   MEBT-010:Vac-VEG-01100
    *   MEBT-010:Vac-VGP-01100
*   MEBT-010:Vac-VEG-10001
    *   MEBT-010:Vac-VGP-20000
    *   MEBT-010:Vac-VGC-10000
    *   MEBT-010:Vac-VGC-30000
*   MEBT-010:Vac-VEG-10002
    *   MEBT-010:Vac-VGC-40000
