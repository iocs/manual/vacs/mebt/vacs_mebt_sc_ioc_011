#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: MEBT-010:Vac-VEG-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEG-01100, BOARD_A_SERIAL_NUMBER = 1807170606, BOARD_B_SERIAL_NUMBER = 1812040755, BOARD_C_SERIAL_NUMBER = 1812040805, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4001")

#
# Device: MEBT-010:Vac-VGP-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-01100, CHANNEL = A1, CONTROLLERNAME = MEBT-010:Vac-VEG-01100")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-01100, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-01100, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: MEBT-010:Vac-VEG-10001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEG-10001, BOARD_A_SERIAL_NUMBER = 1902131337, BOARD_B_SERIAL_NUMBER = 1812100924, BOARD_C_SERIAL_NUMBER = 1812100728, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4002")

#
# Device: MEBT-010:Vac-VGP-20000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-20000, CHANNEL = A1, CONTROLLERNAME = MEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-20000, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGP-20000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")

#
# Device: MEBT-010:Vac-VGC-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-10000, CHANNEL = B1, CONTROLLERNAME = MEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: MEBT-010:Vac-VGC-30000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-30000, CHANNEL = C1, CONTROLLERNAME = MEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-30000, RELAY = 1, RELAY_DESC = 'not wired'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-30000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-30000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-30000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: MEBT-010:Vac-VEG-10002
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = MEBT-010:Vac-VEG-10002, BOARD_A_SERIAL_NUMBER = 1902131009, BOARD_B_SERIAL_NUMBER = 1812100811, BOARD_C_SERIAL_NUMBER = 1812100748, IPADDR = moxa-vac-mebt.tn.esss.lu.se, PORT = 4003")

#
# Device: MEBT-010:Vac-VGC-40000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-40000, CHANNEL = B1, CONTROLLERNAME = MEBT-010:Vac-VEG-10002")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-40000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-40000, RELAY = 2, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-40000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = MEBT-010:Vac-VGC-40000, RELAY = 4, RELAY_DESC = 'not wired'")
